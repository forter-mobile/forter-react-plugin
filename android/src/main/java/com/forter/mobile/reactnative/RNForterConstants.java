package com.forter.mobile.reactnative;

/**
 * Created by maxim on 11/17/16.
 */

public class RNForterConstants {
    final static String NO_SITE_ID_FOUND = "SiteID is empty or missing";
    final static String NO_MOBILE_UID_FOUND = "MobileUID is empty or missing";
    final static String SUCCESS   = "Success";

    final static  String FORTER_TOKEN_UPDATE = "forterTokenUpdate";
}

