#import "RNForter.h"

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import "RCTBridgeModule.h"
#endif

#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#else
#import "RCTBridge.h"
#endif

#if __has_include(<ForterSDK/ForterSDK.h>)
#import <ForterSDK/ForterSDK.h>
#else
#import "ForterSDK.h"
#endif

#if __has_include(<ForterSDK/ForterTokenListener.h>)
#import <ForterSDK/ForterTokenListener.h>
#else
#import "ForterTokenListener.h"
#endif

@implementation RNForter

static NSString *const NO_SITE_ID_FOUND             = @"SiteID is empty or missing";
static NSString *const NO_MOBILE_UID_FOUND          = @"MobileUID is empty or missing";
static NSString *const SUCCESS                      = @"Success";
static ForterTokenListener* listener;
static bool isInitialized = NO;

RCT_EXPORT_MODULE();

- (id)init {
    self = [super init];
    if (self != nil) {
        NSLog(@"[ForterSDK] Setting up an RNForter instance");
        if (listener == nil) {
            listener = [[ForterTokenListener alloc] initWithUpdatesHandler:^(NSString *forterMobileUid) {
                [self sendEventWithName:@"forterTokenUpdate" body:@{@"forterMobileUID": forterMobileUid}];
            }];

            [ForterSDK registerForterTokenListener: listener];
        }

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleBridgeWillReloadNotification:)
                                                     name:RCTBridgeWillReloadNotification
                                                   object:nil];
    }
    return self;
}

- (void)handleBridgeWillReloadNotification:(NSNotification *)notification {
    //Register to hot reloads and release resources
    [ForterSDK unregisterForterTokenListener: listener];
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

RCT_EXPORT_METHOD(initSdk:(NSString*)siteId
                  mobileUid:(NSString*)mobileUid
                  successCallback:(RCTResponseSenderBlock)successCallback
                  errorCallback:(RCTResponseErrorBlock)errorCallback) {
  NSError* error = nil;

  if (isInitialized) {
      return;
  }

  if (!siteId || [siteId isEqualToString:@""]) {
      error = [NSError errorWithDomain:NO_SITE_ID_FOUND code:0 userInfo:nil];

  } else if (!mobileUid || [mobileUid isEqualToString:@""]) {
      error = [NSError errorWithDomain:NO_MOBILE_UID_FOUND code:1 userInfo:nil];
  }

  if (error != nil) {
      errorCallback(error);
  } else {
      [ForterSDK registerForterTokenListener:listener];
      [ForterSDK setupWithDeviceUid:mobileUid siteId:siteId];
      [[ForterSDK sharedInstance] setDeviceUniqueIdentifier:mobileUid];
      successCallback(@[SUCCESS]);
      isInitialized = YES;
  }
}

RCT_EXPORT_METHOD(getForterToken:(RCTResponseSenderBlock)successCallback
                  errorCallback:(RCTResponseErrorBlock)errorCallback) {
    NSError* error = nil;
    NSString* forterToken = [ForterSDK getForterToken: &error];
    if (error != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            errorCallback(@[error]);
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            successCallback(@[forterToken]);
        });
    }
}

RCT_EXPORT_METHOD(getDeviceUniqueID:(RCTResponseSenderBlock)callback) {
  dispatch_async(dispatch_get_main_queue(), ^{
    callback(@[[[[UIDevice currentDevice] identifierForVendor] UUIDString]]);
  });
}

RCT_EXPORT_METHOD(setAccountIdentifier:(NSString*)accountId
                  withAccountType:(NSString*)accountType) {
  FTRSDKAccountIdType sdkAccountType = [self getMatchingAccountType:accountType];
  [[ForterSDK sharedInstance] setAccountIdentifier:accountId withType:sdkAccountType];
}

RCT_EXPORT_METHOD(trackNavigation:(NSString*)screenName
                  navigationType:(NSString*)navigationType) {
  FTRSDKNavigationType sdkNavigationType = [self getMatchingNavigationType:navigationType];
  [[ForterSDK sharedInstance] trackNavigation:screenName withType:sdkNavigationType];
}

RCT_EXPORT_METHOD(trackNavigationWithExtraData:(NSString*)screenName
                  navigationType:(NSString*)navigationType
                  itemId:(NSString*)itemId
                  itemCategory:(NSString*)itemCategory
                  otherInfo:(NSString*)otherInfo) {
  FTRSDKNavigationType sdkNavigationType = [self getMatchingNavigationType:navigationType];
  [[ForterSDK sharedInstance] trackNavigation:screenName withType:sdkNavigationType pageId:itemId pageCategory:itemCategory otherInfo:otherInfo];
}

RCT_EXPORT_METHOD(trackAction:(NSString*)actionType) {
  FTRSDKActionType sdkActionType = [self getMatchingActionType:actionType];
  [[ForterSDK sharedInstance] trackAction:sdkActionType];
}

RCT_EXPORT_METHOD(trackActionWithMessage:(NSString*)actionType
                  message:(NSString*)message) {
  FTRSDKActionType sdkActionType = [self getMatchingActionType:actionType];
  [[ForterSDK sharedInstance] trackAction:sdkActionType withMessage:message];
}

RCT_EXPORT_METHOD(trackActionWithJSON:(NSString*)actionType
                  json:(NSDictionary*)json) {
  FTRSDKActionType sdkActionType = [self getMatchingActionType:actionType];
  [[ForterSDK sharedInstance] trackAction:sdkActionType withData:json];
}

RCT_EXPORT_METHOD(trackCurrentLocation:(float)longitude
                  latitude:(float)latitude) {
  [[ForterSDK sharedInstance] didUpdateLocationLatitude:latitude longitude:longitude altitude:0.0];
}

RCT_EXPORT_METHOD(setDevLogsEnabled) {
  [ForterSDK setDevLogsEnabled:TRUE];
}

RCT_EXPORT_METHOD(getSDKVersionSignature:(RCTResponseSenderBlock)callback) {
  callback(@[[NSNull null], [ForterSDK getSDKVersionSignature]]);
}

- (FTRSDKActionType)getMatchingActionType:(NSString *)forString {
  if ([forString isEqualToString:@"TAP"]) {
    return FTRSDKActionTypeTap;
  } else if ([forString isEqualToString:@"CLIPBOARD"]) {
    return FTRSDKActionTypeClipboard;
  } else if ([forString isEqualToString:@"TYPING"]) {
    return FTRSDKActionTypeTyping;
  } else if ([forString isEqualToString:@"ADD_TO_CART"]) {
    return FTRSDKActionTypeAddToCart;
  } else if ([forString isEqualToString:@"REMOVE_FROM_CART"]) {
    return FTRSDKActionTypeRemoveFromCart;
  } else if ([forString isEqualToString:@"ACCEPTED_PROMOTION"]) {
    return FTRSDKActionTypeAcceptedPromotion;
  } else if ([forString isEqualToString:@"ACCEPTED_TOS"]) {
    return FTRSDKActionTypeAcceptedTos;
  } else if ([forString isEqualToString:@"ACCOUNT_LOGIN"]) {
    return FTRSDKActionTypeAccountLogin;
  } else if ([forString isEqualToString:@"ACCOUNT_LOGOUT"]) {
    return FTRSDKActionTypeAccountLogout;
  } else if ([forString isEqualToString:@"ACCOUNT_ID_ADDED"]) {
    return FTRSDKActionTypeAccountIdAdded;
  } else if ([forString isEqualToString:@"PAYMENT_INFO"]) {
    return FTRSDKActionTypePaymentInfo;
  } else if ([forString isEqualToString:@"SHARE"]) {
    return FTRSDKActionTypeShare;
  } else if ([forString isEqualToString:@"CONFIGURATION_UPDATE"]) {
    return FTRSDKActionTypeConfigurationUpdate;
  } else if ([forString isEqualToString:@"APP_ACTIVE"]) {
    return FTRSDKActionTypeAppActive;
  } else if ([forString isEqualToString:@"APP_PAUSE"]) {
    return FTRSDKActionTypeAppPause;
  } else if ([forString isEqualToString:@"RATE"]) {
    return FTRSDKActionTypeRate;
  } else if ([forString isEqualToString:@"IS_JAILBROKEN"]) {
    return FTRSDKActionTypeIsJailbroken;
  } else if ([forString isEqualToString:@"SEARCH_QUERY"]) {
    return FTRSDKActionTypeSearchQuery;
  } else if ([forString isEqualToString:@"REFERRER"]) {
    return FTRSDKActionTypeReferrer;
  } else if ([forString isEqualToString:@"WEBVIEW_TOKEN"]) {
    return FTRSDKActionTypeWebviewToken;
  } else {
    return FTRSDKActionTypeOther;
  }
}

- (FTRSDKNavigationType)getMatchingNavigationType:(NSString *)forString {
  if ([forString isEqualToString:@"PRODUCT"]) {
    return FTRSDKNavigationTypeProduct;
  } else if ([forString isEqualToString:@"ACCOUNT"]) {
    return FTRSDKNavigationTypeAccount;
  } else if ([forString isEqualToString:@"SEARCH"]) {
    return FTRSDKNavigationTypeSearch;
  } else if ([forString isEqualToString:@"CHECKOUT"]) {
    return FTRSDKNavigationTypeCheckout;
  } else if ([forString isEqualToString:@"CART"]) {
    return FTRSDKNavigationTypeCart;
  } else if ([forString isEqualToString:@"HELP"]) {
    return FTRSDKNavigationTypeHelp;
  } else {
    return FTRSDKNavigationTypeApp;
  }
}

- (FTRSDKAccountIdType)getMatchingAccountType:(NSString *)forString {
  if ([forString isEqualToString:@"MERCHANT"]) {
    return FTRSDKAccountIdTypeMerchant;
  } else if ([forString isEqualToString:@"FACEBOOK"]) {
    return FTRSDKAccountIdTypeFacebook;
  } else if ([forString isEqualToString:@"GOOGLE"]) {
    return FTRSDKAccountIdTypeGoogle;
  } else if ([forString isEqualToString:@"TWITTER"]) {
    return FTRSDKAccountIdTypeTwitter;
  } else if ([forString isEqualToString:@"APPLE_IDFA"]) {
    return FTRSDKAccountIdTypeAppleIDFA;
  } else {
    return FTRSDKAccountIdTypeOther;
  }
}

- (NSArray<NSString *> *)supportedEvents {
    return @[@"forterTokenUpdate"];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:RCTBridgeWillReloadNotification
                                                      object:nil];}


@end
